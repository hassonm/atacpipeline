#! /bin/bash

# grab all .sra files, submit the sra -> fastq conversion jobs
#all_sra_files=$(find /cbio/cllab/nobackup/zamparol/heme_ATAC -name *.sra)

# grab only those files that failed
fails=$(cat /cbio/cllab/home/zamparol/redo_these.txt)

for f in $fails;
do
   bsub -env "all, SRA=$f" < submit_sra_convert_job.lsf
   #qsub -v SRA=$f submit_sra_convert_job.sh
   sleep 1
done


