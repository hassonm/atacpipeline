#! /bin/bash

# grab all cell types, submit the fastq -> bam conversion jobs
all_cell_types=$(ls -1 /cbio/cllab/nobackup/zamparol/heme_ATAC/data/fastq)

for f in $all_cell_types;
do
   bsub -env "all, CT=$f" < submit_fastq_to_bam.lsf
   qsub -v CT=$f submit_fastq_to_bam.sh
   sleep 1
done
