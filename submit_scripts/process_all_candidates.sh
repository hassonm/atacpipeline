#! /bin/bash

# grab all cell types, submit the peaks -> atlas conversion jobs
set -- $(ls /data/leslie/zamparol/heme_ATAC/data/peaks/)

# submit first candidates_to_peaks job
bsub -env "all, CT=$1, BOSS=yes" < submit_candidates_to_celltypes.lsf

shift
for a; do
    #qsub -v CT=$a submit_candidates_to_atlas.sh
    bsub -env "all, CT=$a" < submit_candidates_to_celltypes.lsf
    shift
done

