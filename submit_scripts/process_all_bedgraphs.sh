#! /bin/bash

# grab all cell types, submit the peaks -> atlas conversion jobs

for type in $(ls /data/leslie/zamparol/heme_ATAC/data/peaks/)
do
	bsub -env "all, CT=$type" < submit_generate_merged_bedgraph.lsf
    #qsub -v CT=$type submit_generate_merged_bedgraph.sh
done

