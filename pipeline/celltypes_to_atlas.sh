#!/bin/bash

### try to catch a tonne of errors (h/t Michael Hoffman)
set -o nounset -o pipefail -o errexit

usage="celltypes_to_atlas.sh -- compile set of IDR validated peaks from each celltype into one atlas
Usage:
   bash celltypes_to_atlas.sh [-h] -b base dir -p peaks root dir [-t threshold] [-f organism] 

   arguments:
    -h  show this help text
    -b  base directory for the project.  data root dir should be below here. (e.g ~/projects/myproject)
    -p  path to output base directory for peaks (e.g 'data/peaks' means the output will end up in base/data/peaks)
    -t  threshold on IDR score for retaining peaks.  IDR score thresholds range from 0 (lowest) to 1000 (highest).  Default is 830 (corresponds to P_adj == 0.01)
    -f  name of the reference genome for this experiment, corresponds to a bed file containing omitted regions (e.g hg19, mm10, ce10)
"
declare option
declare OPTARG
declare base
declare peaks
declare omit
declare threshold

while getopts b:p:f:t:h option; do
  case "$option" in
    h) echo "$usage"
       exit
       ;;
    b) base=$OPTARG
       ;;
    p) peaks=$OPTARG
       ;;
    f) omit=$OPTARG
       ;;
    t) threshold=$OPTARG
       ;;
    :) printf "missing argument for -%s\n" "$OPTARG" >&2
       echo "$usage" >&2
       exit
       ;;
   \?) printf "illegal option: -%s\n" "$OPTARG" >&2
       echo "$usage" >&2
       exit
       ;;
  esac
done
shift $((OPTIND - 1))

peaks_root="$base/$peaks"
scripts_dir=`pwd`
omit_dir="$(dirname `pwd`)/omitted_regions"

### ensure all args are at least plausibly correct
[[ -d "$base" ]] || { echo "Error: $base is not a directory"; exit 1; }
[[ -d "$peaks_root" ]] || { echo "Error: $peaks_root is not a directory"; exit 1; }

### debugging info
echo "   using $peaks_root for peaks root dir"
echo "   using $scripts_dir for the scripts dir"

echo "extracting reproducible peaks for all replicates"

### extract reproducible peaks for each pair of replicates of the timepoint (IDR score > 830 <=> IDR < 0.01)
cd $peaks_root

### if threshold is set, use it, otherwise keep the default
if [ -z ${threshold+x} ]; then
  threshold=830
fi

for peaks in $(find . -name *IDR_ranked_peaks.broadPeak)
do
	outfile="$(basename $(dirname $peaks))_reproducible_peaks.tmp"
	outdir=$(dirname $peaks)
	cat $peaks | awk -v thresh="$threshold" '$5 >= thresh' | cut -f1-3 >> $outdir/$outfile
done

### make the unique peaks for each celltype
echo "extracting an atlas for each celltype"

for peaks in $(find . -name *_reproducible_peaks.tmp)
do
  outfile="$(basename $(dirname $peaks))_reproducible_peaks.bed"
  outdir=$(dirname $peaks)
  cat $peaks | sort -k 1,1 -k2,2n - | uniq > $outdir/$outfile
done

### filter each set of peaks in each celltype to omit regions
### commonly accessible but without any biological specificity
if [ -z ${omit+x} ]; then
  echo "not omitting any peaks..."
else
  echo "omitting peaks from list corresponding to $omit reference"
  omit_file="$omit_dir/$omit".blacklist.bed
  [[ -f "$omit_file" ]] || { echo "Error: $omit_file is not a file"; exit 1; }

  for peaks in $(find . -name *_reproducible_peaks.bed)
  do
    # just overwrite same name, use bedtools get regions that do not overlap omitted regions
    prefix=$(dirname $peaks)
    outfile=$(basename $peaks | sed -e 's/peaks.bed/peaks_omitted.bed/')
    bedtools intersect -a $peaks -b $omit_file -v > $prefix/$outfile
    mv $prefix/$outfile $peaks
  done
fi


### merge all reproducible peaks from each celltype into one atlas bedfile
echo "merging each celltype into a unified atlas"
preoutfile="$peaks_root/all_celltypes_peak_atlas.tmp"

for peaks in $(find . -name *_reproducible_peaks.bed)
do
	cat $peaks >> $preoutfile
done

### take the set of all peaks, clean up .tmp files
outfile="$peaks_root/all_celltypes_peak_atlas.bed"
cat $preoutfile | sort -k 1,1 -k2,2n - | uniq > $outfile
echo "cleaning up"
find . -name *.tmp -delete
echo "done"