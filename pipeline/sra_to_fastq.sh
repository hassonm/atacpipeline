#! /bin/bash

### try to catch a tonne of errors (h/t Michael Hoffman)
set -o nounset -o pipefail -o errexit


### parse arguments:

usage="sra_to_fastq.sh -- pipeline to convert .sra files to fastq files.  Conversion is done
in place, respecting the existing directory structure.
Usage:
   bash sra_to_fastq.sh [-h] -b base dir -f myfile

   arguments:
    -h  show this help text
    -b  base directory for the project.
    -f  unpack just this file
    
"
declare option
declare OPTARG
declare base
declare mysra

while getopts hf:b: option; do
  case "$option" in
    h) echo "$usage"
       exit
       ;;
    b) base=$OPTARG
       ;;
    f) mysra=$OPTARG
       ;;
    :) printf "missing argument for -%s\n" "$OPTARG" >&2
       echo "$usage" >&2
       exit
       ;;
   \?) printf "illegal option: -%s\n" "$OPTARG" >&2
       echo "$usage" >&2
       exit
       ;;
  esac
done
shift $((OPTIND - 1))


### ensure all args are at least plausibly correct
[[ -d "$base" ]] || { echo "Error: $base is not a directory"; exit 1; }

echo "starting pipeline..."

### convert sra to fastq
if [ -z ${mysra+x} ]; then 
    echo "convert .sra to .fastq files?"
    select do_conversion in "y" "n";
    do
    if [ "$do_conversion" == "y" ]; then

        find $base -name *.sra -execdir fastq-dump --gzip --skip-technical  --readids --dumpbase --split-files --clip {} \+

    fi
    break;
    done
else 
    echo "converting $mysra ..."
    mydir=$(dirname $mysra)
    cd $mydir
    fastq-dump --gzip --skip-technical  --readids --dumpbase --split-files --clip $mysra
    echo "conversion is done"
fi

echo "Remove .sra files?"
if [ -z ${mysra+x} ]; then 
  select rm_bam in "y" "n";
  do
	 if [ "$rm_bam" == "y" ]; then
		  cd "$base"
		  find . -name *.sra -delete
	 fi
	break;
  done	
else
  echo "not deleting .sra file, do it manually."
  #rm $mysra
fi

