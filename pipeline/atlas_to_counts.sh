#!/bin/bash

### try to catch a tonne of errors (h/t Michael Hoffman)
set -o nounset -o pipefail -o errexit

usage="atlas_to_counts.sh -- script to take a peaks atlas and produce counts for each peak and replicate and timepoint\
Usage:
   bash atlas_to_counts.sh [-h] -b base dir -d data root dir -a bams root dir -p peak atlas bed file -f fine grained peak atlas bed file -c counts root dir

   arguments:
    -h  show this help text
    -b  base directory for the project.  data root dir should be below here. (e.g ~/projects/myproject)
    -a  path extension below base directory where the bams data resides (e.g 'bam' means the data is in base/data/bam)
    -p  path to peak atlas bed file (e.g 'data/peaks/my_peak_atlas.bed')
    -f  path to fine grained (e.g each peak split into 20bp mini-bins) bed file (e.g 'data/peaks/all_timepts_fine_atlas_cut.bed')
    -c  path to output base directory for count data (e.g 'counts' means output is written to base/data/counts)

"
declare option
declare OPTARG
declare base
declare bams
declare peaks
declare atlas
declare counts
declare fineatlas

while getopts b:a:p:c:f:h option; do
  case "$option" in
    h) echo "$usage"
       exit
       ;;
    b) base=$OPTARG
       ;;
    a) bams=$OPTARG
       ;;
    p) atlas=$OPTARG
       ;;
    c) counts=$OPTARG
       ;;
    f) fineatlas=$OPTARG
       ;;
    :) printf "missing argument for -%s\n" "$OPTARG" >&2
       echo "$usage" >&2
       exit
       ;;
   \?) printf "illegal option: -%s\n" "$OPTARG" >&2
       echo "$usage" >&2
       exit
       ;;
  esac
done
shift $((OPTIND - 1))

data_root="$base/data"
bams_root="$data_root/bam"
atlas="$data_root/$atlas"
fineatlas="$data_root/$fineatlas"
counts_root="$data_root/$counts"
scripts_dir=`pwd`

### ensure all args are at least plausibly correct
[[ -d "$base" ]] || { echo "Error: $base is not a directory"; exit 1; }
[[ -d "$bams_root" ]] || { echo "Error: $bams_root is not a directory"; exit 1; }
[[ -f "$atlas" ]] || { echo "Error: $atlas is not a file"; exit 1; }
[[ -f "$fineatlas" ]] || { echo "Error: $fineatlas is not a file"; exit 1; }
[[ -d "$counts_root" ]] || { echo "Error: $counts_root is not a directory"; exit 1; }

echo "starting pipeline..."

### debugging info
echo "   using $data_root for data root dir"
echo "   using $bams_root for bams root dir"
echo "   using $atlas for atlas file"
echo "   using $fineatlas for fine-grained atlas file"
echo "   using $counts_root for the counts root dir"

# Users/zamparol/projects/thymus/data/bam/./day7/Tecs1_sorted.bam
make_counts(){
	counts_root=$2
	atlas=$3
	timept=$(basename $(dirname $1))
	out_name=$(basename $1 | sed -e 's/sorted.bam/raw_read_counts.bed/g')
	bedtools coverage -sorted -b $1 -a $atlas -counts > $counts_root/$timept/$out_name
}

export -f make_counts

### compute the number of reads which support each peak in the atlas for all timepoints and replicates
echo "compute read support for atlas peak regions?"
select do_counts in "y" "n";
do
	if [ "$do_counts" == "y" ]; then
		cd $bams_root
		bams=$(find . -name Tecs*_sorted.bam | sed -e "s/^\.//" | awk -v br="$bams_root" '{print br$1}')
		# count supports
		parallel -j 6 --progress --xapply make_counts ::: $bams ::: $counts_root ::: $atlas
	fi
	break;
done


### compute the per 20bp read overlap for each extended peak
# perform 'bedtools coverage' on this new fine-grained atlas
# Users/zamparol/projects/thymus/data/bam/./day7/Tecs1_sorted.bam
make_fine_counts(){
  counts_root=$2
  atlas=$3
  timept=$(basename $(dirname $1))
  out_name=$(basename $1 | sed -e 's/sorted.bam/fine_grained_read_counts.bed/g')
  bedtools coverage -sorted -b $1 -a $atlas -counts > $counts_root/$timept/$out_name
}

export -f make_fine_counts

### compute the number of reads which support each peak in the atlas for all timepoints and replicates
echo "compute read support for *fine grained* atlas peak regions?"
select do_fine_counts in "y" "n";
do
  if [ "$do_fine_counts" == "y" ]; then
    cd $bams_root
    bams=$(find . -name Tecs*_sorted.bam | sed -e "s/^\.//" | awk -v br="$bams_root" '{print br$1}')
    # count supports
    parallel -j 6 --progress --xapply make_fine_counts ::: $bams ::: $counts_root ::: $fineatlas
  fi
  break;
done


#bedtools intersect -b mm10_exons_sorted.bed -b mm10_introns_sorted.bed -b mm10_1kb_upstream_sorted.bed -a ../peaks/all_timepts_peak_atlas.bed -wao -sortout | awk -v ORF="\t" '{print $1,$2,$3,$8,$11}' > ../peaks/annotated_peak_atlas.bed
