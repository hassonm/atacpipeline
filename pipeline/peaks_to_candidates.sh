#!/bin/bash

### try to catch a tonne of errors (h/t Michael Hoffman)
set -o nounset -o pipefail -o errexit

usage="peaks_to_candidates.sh -- pipeline to convert peaks called on individual timepoint bam files to set of common peak regions
which will be vetted later via IDR on individual cell types.  Optionally, filter out regions that appear on a list of common regions
that are universally accessible regardless of the experiment.
Usage:
   bash peaks_to_candidates.sh [-h] -b base dir -a bams root dir -p peaks root dir 

   arguments:
    -h  show this help text
    -b  base directory for the project.  data root dir should be below here. (e.g ~/projects/myproject)
    -a  path extension below base directory where the bams data resides (e.g 'bam' means the data is in base/data/bam)
    -p  path to output base directory for peaks (e.g 'data/peaks' means the output will end up in base/data/peaks)
"
declare option
declare OPTARG
declare base
declare bams
declare peaks
declare celltype

while getopts b:a:p:h option; do
  case "$option" in
    h) echo "$usage"
       exit
       ;;
    b) base=$OPTARG
       ;;
    a) bams=$OPTARG
       ;;
    p) peaks=$OPTARG
       ;;   
    :) printf "missing argument for -%s\n" "$OPTARG" >&2
       echo "$usage" >&2
       exit
       ;;
   \?) printf "illegal option: -%s\n" "$OPTARG" >&2
       echo "$usage" >&2
       exit
       ;;
  esac
done
shift $((OPTIND - 1))

bams_root="$base/$bams"
peaks_root="$base/$peaks"
scripts_dir=`pwd`


### ensure all args are at least plausibly correct
[[ -d "$base" ]] || { echo "Error: $base is not a directory"; exit 1; }
[[ -d "$bams_root" ]] || { echo "Error: $bams_root is not a directory"; exit 1; }
[[ -d "$peaks_root" ]] || { echo "Error: $peaks_root is not a directory"; exit 1; }

echo "starting pipeline..."

### debugging info
echo "   using $bams_root for bams root dir"
echo "   using $peaks_root for peaks root dir"
echo "   using $scripts_dir for the scripts dir"


### merge all bams from all types or timepoints, index & sort the merged bam

echo "merging all bams from all types/timepoints to form meta bam"
cd $bams_root
bams=$(find . -name *.bam)
samtools merge -@ 16 all_merged.bam $bams
samtools index all_merged.bam
	

### call peaks on the merged bam

echo "calling peaks on merged bam file"
cd $bams_root
mkdir -p $peaks_root/merged
set +o nounset
source activate py2
macs2 callpeak -t $bams_root/all_merged.bam -f BAM -n all_merged -B --outdir $peaks_root/merged -g hs -p 0.2 --nomodel --extsize 76 --shift 0 --keep-dup all --cutoff-analysis --call-summits 2>$peaks_root/merged/.macs
source deactivate
set -o nounset
