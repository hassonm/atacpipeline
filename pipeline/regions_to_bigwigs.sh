#!/bin/bash

### try to catch a tonne of errors (h/t Michael Hoffman)
set -o nounset -o pipefail -o errexit

usage="regions_to_bigwigs.sh -- script to take a set of peaks in different regions (encoded as bed files) peaks atlas and produce bigwig files for display in IGV or the like\
Usage:
   bash regions_to_bigwigs.sh [-h] -b base dir -a bams root dir -r regions dir 
   arguments:
    -h  show this help text
    -b  base directory for the project.  data root dir should be below here. (e.g ~/projects/myproject)
    -a  path extension below base directory where the bams data resides (e.g 'bam' means the data is in base/data/bam)
    -r  path to regions files (e.g 'data/regions')
"
declare option
declare OPTARG
declare base
declare bams
declare regions

while getopts b:a:r:h option; do
  case "$option" in
    h) echo "$usage"
       exit
       ;;
    b) base=$OPTARG
       ;;
    a) bams=$OPTARG
       ;;
    r) regions=$OPTARG
       ;;
    :) printf "missing argument for -%s\n" "$OPTARG" >&2
       echo "$usage" >&2
       exit
       ;;
   \?) printf "illegal option: -%s\n" "$OPTARG" >&2
       echo "$usage" >&2
       exit
       ;;
  esac
done
shift $((OPTIND - 1))

data_root="$base/data"
bams_root="$data_root/bam"
regions="$data_root/$regions"
scripts_dir=`pwd`

### ensure all args are at least plausibly correct
[[ -d "$base" ]] || { echo "Error: $base is not a directory"; exit 1; }
[[ -d "$bams_root" ]] || { echo "Error: $bams_root is not a directory"; exit 1; }
[[ -d "$regions" ]] || { echo "Error: $regions is not a directory"; exit 1; }

echo "starting pipeline..."

### debugging info
echo "   using $data_root for data root dir"
echo "   using $bams_root for bams root dir"
echo "   using $regions for atlas file"


# need to extract day, replicate number from bam name: /Users/zamparol/projects/thymus/data/bam/day7/Tecs1_sorted.bam
make_intersections(){
  rep_bam=$1
  region_bed=$2
  region_prefix=$(echo $region_bed | cut -d\. -f1)
  rep_name=$(echo $rep_bam | rev | cut -d\/ -f 1 | rev | cut -d\_ -f 1)
  bam_day=$(echo $rep_bam | rev | cut -d\/ -f 2 | rev)
  out_file=$(echo "$region_prefix"_"$bam_day"_"$rep_name".bam)
  bedtools intersect -a $rep_bam -b $region_bed > $out_file
}
export -f make_intersections

### bedtools intersect for bam files in each time point & replicate
echo "compute intersections from each region?"
select do_intersections in "y" "n";
do
  if [ "$do_intersections" == "y" ]; then
    cd $regions
    parallel -j 6 --progress make_intersections ::: $(find $bams_root -name *sorted.bam) ::: *.bed
  fi
  break;
done


make_bedgraphs(){
  input_bam=$1
  output_bg=$(echo $1 | sed -e "s/.bam/.bedgraph/g")
  bedtools genomecov -trackline -bg -ibam $input_bam -g ~/genomes/mm10/mm10.chrom.sizes > $output_bg
}
export -f make_bedgraphs

### bedtools genomecov to find read coverage at each base for each region, rep
echo "compute intersections from each region?"
select do_generate_bg in "y" "n";
do
  if [ "$do_generate_bg" == "y" ]; then
    cd $regions
    my_intersection_bams=$(ls -1 *.bam)
    parallel -j 6 --progress make_bedgraphs ::: $my_intersection_bams
  fi
  break;
done

merge_bedgraphs(){
  prefix=$1
  rep1="$prefix"Tecs1.bedgraph
  rep2="$prefix"Tecs2.bedgraph 
  outfile="$prefix"merged.bedgraph
  bedtools unionbedg -i $rep1 $rep2 > $outfile
}
export -f merge_bedgraphs

### merge bedgraph files from replicates
echo "merge bedgraph files from replicates?"
select do_merge_bg in "y" "n";
do
  if [ "$do_merge_bg" == "y" ]; then
    cd $regions
    my_bgs=$(ls -1 *.bedgraph | cut -dT -f1 | uniq)
    parallel -j 6 --progress merge_bedgraphs ::: $my_bgs
  fi
  break;
done

### tidy up
echo "tidy up intermediate storage?"
select tidy_up in "y" "n";
do
  if [ "$tidy_up" == "y" ]; then
    cd $regions
    rm *.bam
    rm *_Tecs?.bedgraph
  fi
  break;
done


