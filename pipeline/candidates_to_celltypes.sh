#!/bin/bash

### try to catch a tonne of errors (h/t Michael Hoffman)
set -o nounset -o pipefail -o errexit

usage="candidates_to_celltypes.sh -- pipeline to convert candidate peaks called on merged bam to IDR validated peaks
Usage:
   bash candidates_to_celltypes.sh [-h] -b base dir -a bams root dir -p peaks root dir -g celltype

   arguments:
    -h  show this help text
    -b  base directory for the project.  data root dir should be below here. (e.g ~/projects/myproject)
    -a  path extension below base directory where the bams data resides (e.g 'bam' means the data is in base/data/bam)
    -p  path to output base directory for peaks (e.g 'data/peaks' means the output will end up in base/data/peaks)
    -g  subdirectory of bams/peaks directories that is associated with a given celltype.
"
declare option
declare OPTARG
declare base
declare bams
declare peaks
declare celltype

while getopts b:a:p:g:h option; do
  case "$option" in
    h) echo "$usage"
       exit
       ;;
    b) base=$OPTARG
       ;;
    a) bams=$OPTARG
       ;;
    p) peaks=$OPTARG
       ;;
    g) celltype=$OPTARG
	   ;;
    :) printf "missing argument for -%s\n" "$OPTARG" >&2
       echo "$usage" >&2
       exit
       ;;
   \?) printf "illegal option: -%s\n" "$OPTARG" >&2
       echo "$usage" >&2
       exit
       ;;
  esac
done
shift $((OPTIND - 1))

bams_root="$base/$bams"
peaks_root="$base/$peaks"
scripts_dir=`pwd`

### ensure all args are at least plausibly correct
[[ -d "$base" ]] || { echo "Error: $base is not a directory"; exit 1; }
[[ -d "$bams_root" ]] || { echo "Error: $bams_root is not a directory"; exit 1; }
[[ -d "$peaks_root" ]] || { echo "Error: $peaks_root is not a directory"; exit 1; }

echo "starting pipeline..."

### debugging info
echo "   using $bams_root for bams root dir"
echo "   using $peaks_root for peaks root dir"
echo "   using $scripts_dir for the scripts dir"

### compute intersections of narrowPeaks on merged bams and bdg of individual replicates
make_intersections(){
	in_name=$(basename $1)
	in_dir=$(dirname $1)
	peaks_root=$2
	out_name=$(basename $1 | sed -e 's/treat_pileup.bdg/treat_merged_peaks.bdg/g' | sed -e 's/control_lambda.bdg/ctrl_merged_peaks.bdg/g')
	bedtools intersect -b $peaks_root/merged/all_merged_peaks.narrowPeak -a $1 > $in_dir/$out_name
}

export -f make_intersections

if [ -z ${celltype+x} ]; then
	echo "compute intersections of candidate merged peak regions on individual replicates?"
	select do_intersections in "y" "n";
	do
		if [ "$do_intersections" == "y" ]; then
			cd $peaks_root
			treat_pileups=$(find . -name Tecs*_treat_pileup.bdg)
			ctrl_pileups=$(find . -name Tecs*_control_lambda.bdg)
			# compute intersections
			parallel --gnu -j 6 --progress --xapply make_intersections ::: $treat_pileups ::: $peaks_root
			parallel --gnu -j 6 --progress --xapply make_intersections ::: $ctrl_pileups ::: $peaks_root
		fi
		break;
	done
else
	echo "computing intersections of candidate merged peak regions on individual replicates"
	cd $peaks_root/$celltype
	treat_pileups=$(find . -name *treat_pileup.bdg)
	ctrl_pileups=$(find . -name *control_lambda.bdg)
	# compute intersections
	parallel --gnu -j 6 --xapply make_intersections ::: $treat_pileups ::: $peaks_root
	parallel --gnu -j 6 --xapply make_intersections ::: $ctrl_pileups ::: $peaks_root
fi


### compute p-values from intersections, and assign to peaks 
compute_pvals(){
	outdir=$(dirname $1)
	outfile=$(basename $1 | sed -e 's/_treat_merged_peaks.bdg/_pvalue_ppois.bdg/g')
	macs2 bdgcmp -t $1 -c $2 -m ppois -o $outdir/$outfile
	outsorted=$(echo $outfile | sed -e 's/_pvalue_ppois.bdg/_pvalue_ppois_sorted.bdg/g')
    bedtools sort -i $outdir/$outfile > $outdir/$outsorted
}

export -f compute_pvals

if [ -z ${celltype+x} ]; then
	echo "compute p-values from intersections?"
	select do_compute_pvals in "y" "n";
	do
		if [ "$do_compute_pvals" == "y" ]; then
			cd $peaks_root
			treats=$(find . -name Tecs*_treat_merged_peaks.bdg)
			ctrls=$(find . -name Tecs*_ctrl_merged_peaks.bdg)
			parallel --gnu -j 6 --progress --xapply compute_pvals ::: $treats ::: $ctrls
	   	fi
	   	break;
	done
else
	echo "computing p-values from intersections"
	cd $peaks_root/$celltype
	treats=$(find . -name *_treat_merged_peaks.bdg)
	ctrls=$(find . -name *_ctrl_merged_peaks.bdg)
	set +o nounset
	source activate py2
	parallel --gnu -j 6 --xapply compute_pvals ::: $treats ::: $ctrls
	source deactivate
	set -o nounset
fi

### remove intermediate storage of bedgraph files
if [ -z ${celltype+x} ]; then
	echo "remove intermediate bedgraph storage?"
	select do_bedgraph_cleanup in "y" "n";
	do
		if [ "$do_bedgraph_cleanup" == "y" ]; then
			find . -name Tecs*_treat_merged_peaks.bdg -delete
			find . -name Tecs*_control_merged_peaks.bdg -delete
			find . -name Tecs*_pvalue_ppois.bdg -delete
		fi
		break;
	done
else
	echo "removing intermediate bedgraph storage"
	cd $peaks_root/$celltype
	find . -name *treat_merged_peaks.bdg -delete
	find . -name *control_merged_peaks.bdg -delete	
	find . -name *pvalue_ppois.bdg -delete
fi


### assign the p-value for each peak in the atlas: for each peak, 
### find all overlapping regions in the timept/rep, get max of pvals, assign it to peak.
assign_pvals(){
	outdir=$(dirname $(dirname $1))
	outfile=$(basename $1 | sed -e 's/pvalue_ppois_sorted.bdg/aligned_peaks.broadPeak/g')
	### collect all p-values for all sub-regions overlapping each aligned candidate peak; 
	### if there are none, replace the resultant '.' values with min val
	minval=$(bedtools map -a $2 -b $1 -c 4 -o max | cut -f11 | sort | head -n 200 | sort -r | head -n 1)
	echo "minval for file $outdir/$outfile is $minval"
	### also slap a -1 on the end for the q-value, so this can be represented as a broadPeak
	# 
	# awk -F $'\t' {' if ($8 == ".") {for(i=1;i<=7;++i) print "$i\t"; print "$minval\t -99";} else print "$0\t-1";'}
	bedtools map -a $2 -b $1 -c 4 -o max | cut -f1-7,11 | awk -F $'\t' -v minval="$minval" {' if ($8 == ".") {$8 = minval; print $0"\t-1";} else {print $0"\t-1";} '} > $outdir/$outfile
    # bedtools map -a $2 -b $1 -c 4 -o max | cut -f1-7,11 | awk -F $'\t' {' if ($8 == ".") {$8 = "$minval";} {for(i=1;i<=8;++i)print $i}"\t-1"'} > $outdir/$outfile
}

export -f assign_pvals

if [ -z $celltype+x} ]; then
	echo "assign p-values from intersections to peaks?"
	select do_assign_pvals in "y" "n";
	do
		if [ "$do_assign_pvals" == "y" ]; then
			cd $peaks_root
			treats=$(find . -name Tecs*_ppois_sorted.bdg)
			merged_peaks=$(find . -name all_merged_peaks.narrowPeak)
			parallel --gnu -j 6 --progress --xapply assign_pvals ::: $treats ::: $merged_peaks
	   	fi
	   	break;
	done
else
	echo "assigning p-values from intersections to peaks"
	cd $peaks_root/$celltype
	treats=$(find . -name *_ppois_sorted.bdg)
	merged_peaks=$(find .. -name all_merged_peaks.narrowPeak)
	parallel --gnu -j 6 --xapply assign_pvals ::: $treats ::: $merged_peaks

fi

### run IDR on each timepoint using the called peaks on the merged bam as the arg to '--peak-list' for IDR
run_idr(){
	outdir=$(dirname $(dirname $1))
    prefix_a=$(echo $(basename $1) | sed -e 's/aligned_peaks.broadPeak//g')
    prefix_b=$(echo $(basename $2) | sed -e 's/aligned_peaks.broadPeak//g')
	outfile="$prefix_a-$prefix_b-IDR_ranked_peaks.broadPeak"
	idr --verbose --samples $1 $2 --input-file-type broadPeak --rank p.value -o $outdir/$outfile 2>$outdir/idr-errors.txt
}

export -f run_idr

if [ -z ${celltype+x} ]; then
	echo "run IDR on each timepoint's aligned peaks?"
	select do_idr_timepts in "y" "n";
	do 
		if [ "$do_idr_timepts" == "y" ]; then
			cd $peaks_root
			rep1=$(find . -name Tecs1_aligned_peaks.broadPeak)
			rep2=$(find . -name Tecs2_aligned_peaks.broadPeak)
			parallel --gnu -j 6 --progress --xapply run_idr ::: $rep1 ::: $rep2
		fi
		break;
	done
else
	echo "running IDR tournament on aligned peaks from each celltype"
	cd $peaks_root/$celltype
	set -- $(ls *aligned_peaks.broadPeak)
	for a; do
	    shift
	    for b; do
	    	parallel --gnu -j 6 --xapply run_idr ::: $a ::: $b
		done
	done

fi


### extract reproducible peaks for each pair of replicates of the timepoint (IDR score > 830, i.e., IDR < 0.01)
if [ -z ${celltype+x} ]; then
	echo "extract reproducible peaks for each pair of replicates and merge into atlas?"
	select get_atlas in "y" "n";
	do
		if [ "$get_atlas" == "y" ]; then
			cd $peaks_root
			for peaks in $(find . -name *IDR_ranked_peaks.broadPeak)
			do
				outfile="$(basename $(dirname $peaks))reproducible_peaks.bed"
				outdir=$(dirname $peaks)
				cat $peaks | awk '$5 > 830' | cut -f1-3 | bedtools sort -i - | uniq >$outdir/$outfile
			done
			cat */*reproducible_peaks.bed | bedtools sort -i - | uniq > all_timepts_peak_atlas.bed
		fi
		break;
	done
else
	echo "done"
fi
