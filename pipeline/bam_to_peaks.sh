#!/bin/bash

### try to catch a tonne of errors (h/t Michael Hoffman)
set -o nounset -o pipefail -o errexit

usage="bam_to_peaks.sh -- pipeline to convert bam files measured across different timepoints to IDR validated peaks
Usage:
   bash bam_to_peaks.sh [-h] -b base dir -d data root dir -p put peaks here -g celltype

   arguments:
    -h  show this help text
    -b  base directory for the project.  data root dir should be below here. (e.g ~/projects/myproject)
    -a  path extension below base directory where the bams data resides (e.g 'data/bam' means the data is in base/data/bam)
    -p  path to output base directory for peaks (e.g data/peaks means the output will end up in base/data/peaks)
    -g  sub-directory that is associated with a given celltype (below where all bam files live)
"
declare option
declare OPTARG
declare base
declare bams
declare peaks

while getopts b:a:p:g:h option; do
  case "$option" in
    h) echo "$usage"
       exit
       ;;
    b) base=$OPTARG
       ;;
    a) bams=$OPTARG
       ;;
    p) peaks=$OPTARG
       ;;
    g) celltype=$OPTARG
	   ;;
    :) printf "missing argument for -%s\n" "$OPTARG" >&2
       echo "$usage" >&2
       exit
       ;;
   \?) printf "illegal option: -%s\n" "$OPTARG" >&2
       echo "$usage" >&2
       exit
       ;;
  esac
done
shift $((OPTIND - 1))

bams_root="$base/$bams"
peaks_root="$base/$peaks"
scripts_dir=`pwd`

### ensure all args are at least plausibly correct
[[ -d "$base" ]] || { echo "Error: $base is not a directory"; exit 1; }
[[ -d "$bams_root" ]] || { echo "Error: $bams_root is not a directory"; exit 1; }
[[ -d "$peaks_root" ]] || { echo "Error: $peaks_root is not a directory"; exit 1; }

echo "starting pipeline..."

### debugging info
echo "   using $bams_root for bams root dir"
echo "   using $peaks_root for peaks root dir"
echo "   using $scripts_dir for the scripts dir"


bam_to_bed(){
	filename=$(basename "$1")


	if [[ $1 =~ ^.*\_sorted.bam ]]; then
		outfile=`echo $1 | sed -e 's/_sorted.bam/.bed/g'`
	fi
	if [[ $1 =~ ^.*\.sorted.bam ]]; then
		outfile=`echo $1 | sed -e 's/.sorted.bam/.bed/g'`
	fi
	if [ -z ${outfile+x} ]; then
		echo "Failed to find (and match) any bam files, exiting..." 1>&2
		exit 1
	fi

    bamToBed -i $filename > $outfile
}

export -f bam_to_bed

### convert bam to bed files, shift all elements to account for Tn5 inclusion, move to peaks subfolder


if [ -z ${celltype+x} ]; then
	echo "convert bam to bed files?"
	select do_bamtobed in "y" "n";
	do
		if [ "$do_bamtobed" == "y" ]; then

			for timept in `ls -1 $bams_root`
			do
			    cd "$bams_root/$timept"
			    echo "processing $timept files..."
			    files=$(ls -1 *.bam)
			    parallel --gnu -j 6 --progress --xapply bam_to_bed ::: $files 
			    mv *.bed "$peaks_root/$timept"
			done

		fi
		break;
	done
else
	echo "converting bam to bed files..."
	mydir="$bams_root/$celltype"
	cd $mydir
	if [ ! -d "$peaks_root/$celltype" ]; then
  		mkdir -p "$peaks_root/$celltype"
	fi

	files=$(ls -1 *.bam)
    parallel --gnu -j 6 --progress --xapply bam_to_bed ::: $files 
    mv *.bed "$peaks_root/$celltype"
fi




shift_bed(){
	filename=$1
	scripts=$2
	outfile=`echo $1 | sed -e 's/.bed/_shifted.bed/g'`
	cat $filename | $scripts/adjustBedTn5.sh > $outfile

}

export -f shift_bed

### shift each bed file by appropriate amount to account for presence of Tn5

if [ -z ${celltype+x} ]; then
	echo "shift bed files to account for Tn5?"
	select do_shift in "y" "n";
	do
		if [ "$do_shift" == "y" ]; then

			for timept in `ls -1 $peaks_root`
			do
				cd "$peaks_root/$timept"
				echo "processing $timept files..."
				files=$(ls -1 *.bed)
				parallel --gnu  -j 6 --progress --xapply shift_bed ::: $files ::: $scripts_dir
			done

		fi
		break;
	done
else
	echo "Shifting bed files to account for Tn5..."
	mydir="$peaks_root/$celltype"
	cd $mydir
	files=$(ls -1 *.bed)
	parallel --gnu -j 6 --progress --xapply shift_bed ::: $files ::: $scripts_dir

fi

### remove un-shifted bed files

if [ -z ${celltype+x} ]; then
	echo "remove un-shifted bed files?"
	select do_remove_unshifted in "y" "n";
	do
		if [ "$do_remove_unshifted" == "y" ]; then
			cd $peaks_root
			find . -name *[0-9].bed -delete
		fi
		break;
	done
else
	echo "removing un-shifted bed files..."
	cd $peaks_root
	find . -name *[0-9].bed -delete
fi

run_macs(){
    name=$(echo $1 | sed -e 's/_shifted.bed//g')
    macs2 callpeak -t $1 -f BED -g hs --outdir ./MACS2 --nomodel --shift 0 --extsize 76 --name $name -p 1e-2 -B 2>&1 > ./MACS2/macs2.err
}

export -f run_macs

### run MACS2

if [ -z ${celltype+x} ]; then
	echo "call peaks on each bed file with MACS2?"
	select do_macs in "y" "n";
	do
		if [ "$do_macs" == "y" ]; then

			for timept in `ls -1 $peaks_root`
			do
				cd "$peaks_root/$timept"
				mkdir -p MACS2
				echo "processing $timept files..."
				files=$(ls -1 *.bed)
				parallel --gnu -j 6 --progress --xapply run_macs ::: $files 
			done

		fi
		break;
	done
else
	echo "Calling peaks on each bed file with MACS2..."
	set +o nounset
	source activate py2
	mydir="$peaks_root/$celltype"
	cd $mydir
	mkdir -p MACS2
	files=$(ls -1 *.bed)
	parallel --gnu -j 6  --xapply run_macs ::: $files 
	source deactivate
	set -o nounset
fi
