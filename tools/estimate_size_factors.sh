#!/bin/bash

### get && divide by RPM
# from: https://www.biostars.org/p/129912/
# Pseudo script for normalising to RPM:

# mappedReads = samtools view -c -F 4 input.bam
# scalingFactor = 1000000 / mappedReads
# bedtools genomecov -ibam input.bam -bg -scale scalingFactor -g chrom.sizes > output.bedGraph
# bedGraphToBigWig input.bedGraph chrom.sizes output.bigWig


### try to catch a tonne of errors (h/t Michael Hoffman)
set -o nounset -o pipefail -o errexit

usage="estimate_size_factors.sh -- estmate all size factors for each celltype and replicate
Usage:
   bash estimate_size_factors.sh [-h] -b base dir -a bams root dir -o outfile

   arguments:
    -h  show this help text
    -b  base directory for the project.  data root dir should be below here. (e.g ~/projects/myproject)
    -a  path to output base directory for bams (e.g 'data/bam' means the output will end up in base/data/bam)
    -o  name of the output file
"
declare option
declare OPTARG
declare base
declare bams
declare outfile

while getopts b:a:o:h option; do
  case "$option" in
    h) echo "$usage"
       exit
       ;;
    b) base=$OPTARG
       ;;
    a) bams=$OPTARG
       ;;
    o) outfile=$OPTARG
	   ;;
    :) printf "missing argument for -%s\n" "$OPTARG" >&2
       echo "$usage" >&2
       exit
       ;;
   \?) printf "illegal option: -%s\n" "$OPTARG" >&2
       echo "$usage" >&2
       exit
       ;;
  esac
done
shift $((OPTIND - 1))

bams_root="$base/$bams"
scripts_dir=`pwd`

cd $bams_root
allbams=$(find . -name "*.bam")

estimate_sf(){
  infile=$1
  celltype_repname=$(basename $infile | sed -e 's/_sorted.bam//')
  outfile=$2
  reads=$(samtools view -c -F 4 $infile)
  sf=$(bc <<< "scale=5 ; 1000000 / $reads")
  echo "$celltype_repname, $reads, $sf" >> $outfile
}

export -f estimate_sf

# establish outfile below $base/results
myoutfile="$base/data/results/$outfile"
echo "rep , reads, sizefactor" > $myoutfile

# do the estimation
parallel --gnu -j 7 --progress --xapply estimate_sf ::: $allbams ::: $myoutfile


